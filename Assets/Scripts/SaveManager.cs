using UnityEngine;
using System.IO;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance;
    public string HighScoreName;
    public int HighScore;
    public int m_Points;
    public string m_ScoreName;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        LoadData();
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    [System.Serializable]
    class Save
    {
        public string HighScoreName;
        public int HighScore;
    }

    public void SaveData()
    {
        if (HighScore < m_Points)
        {
            Save data = new Save();
            data.HighScoreName = m_ScoreName;
            data.HighScore = m_Points;

            string json = JsonUtility.ToJson(data);
            File.WriteAllText(Application.persistentDataPath + "/save.json", json);
        }
    }

    public void LoadData()
    {
        string path = Application.persistentDataPath + "/save.json";

        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);

            Save data = JsonUtility.FromJson<Save>(json);

            HighScoreName = data.HighScoreName;
            HighScore = data.HighScore;
            Debug.Log(data.HighScore);
        }
    }
}