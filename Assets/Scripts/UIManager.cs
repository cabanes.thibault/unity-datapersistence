
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class UIManager : MonoBehaviour
{
    private SaveManager sv;
    public InputField Input;
    public Text HighScoreText;
    public Text PlayerName;
    // Start is called before the first frame update
    void Start()
    {
        sv = SaveManager.Instance;
        Input.onValueChanged.AddListener(SetPlayerName);

        if (sv.m_Points > sv.HighScore)
        {
            SetHighScore(sv.m_Points, sv.m_ScoreName);
        } else
        {
            SetHighScore(sv.HighScore, sv.HighScoreName);
        }
    }

    public void SetPlayerName(string arg0)
    {
        SaveManager.Instance.m_ScoreName = arg0;
        PlayerName.text = $"Player Name : {arg0}";
    }

    public void SetHighScore(int score, string name)
    {
        HighScoreText.text = $"High Score : {name} : {score}";
    }

    public void Game()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        SaveManager.Instance.SaveData();

#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit(); // original code to quit Unity player
#endif
    }
}
